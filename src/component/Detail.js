import React from 'react';
import axios from 'axios';
import { Card, CardContent, CardMedia, CardActionArea, Typography } from '@mui/material';


export default class PokemonDetail extends React.Component {
    state = {
        pokemonDetail: [],
        attack1: [],
        attack2: []
    }

    // Hydrate (affecter les données de l'entité avec les données récupérées de la requête) le pokemonDetail dans le state
    setPokemonDetails(datas) {
        const pokemonDetail = {};
        // affectation de pokemonDetail
        pokemonDetail["name"] = this.capitalize(datas.name);
        pokemonDetail["img"] = datas.sprites.other["official-artwork"].front_default;
        pokemonDetail["types"] = this.setTypes(datas.types);
        pokemonDetail["weight"] = this.formatInfo(datas.weight, "kg");
        pokemonDetail["height"] = this.formatInfo(datas.height, "m");
        this.getAttack(datas.moves[Math.floor(Math.random() * datas.moves.length)].move.url);
        this.getAttack(datas.moves[Math.floor(Math.random() * datas.moves.length)].move.url);
        // on met à jour le state avec pokemonDetail
        this.setState({ pokemonDetail });
    }

    // Récupère les données d'une attaque (détails) et elle les affecte à une de nos attaques
    getAttack(url) {
        axios.get(url)
            .then(res => {
                // si il n'y a pas d'attaque 1, alors récupère les détails de l'attaque 1
                if (this.state.attack1.name == null) {
                    this.setState({ attack1: this.getAttackDetails(res.data) })
                }
                // sinon cela sinon que l'attaque 1 existe, et que l'on peut ajouter la 2ème
                else {
                    this.setState({ attack2: this.getAttackDetails(res.data) })
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    // Hydrate (affecter les données de l'entité avec les données récupérées de la requête) l'attaque dans le state
    getAttackDetails(datas) {
        const attack = {};
        attack["name"] = this.capitalize(datas.name);
        attack["power"] = datas.power;
        attack["description"] = this.getNiceFlavorText(datas.flavor_text_entries);
        return attack;
    }

    // recherche dans le tableau le dernier élement en anglais 'en'
    getNiceFlavorText(entries) {
        for (const entry of entries.reverse()) {
            if (entry.language.name === "en") {
                return entry.flavor_text;
            }
        }
    }

    // Met la 1ère lettre de la chaine de caractères en majuscule
    capitalize(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    // méthode qui formalise les types récupérés de l'api
    // si le pokemon n'a qu'un type, on l'écrit normalement
    // sinon on insère un "/" entre les deux noms de types
    setTypes(types) {
        return types.length === 1 ? this.capitalize(types[0].type.name) : this.capitalize(types[0].type.name) + "/" + this.capitalize(types[1].type.name);
    }

    // Formate les unités en les mettant au format universel (kg et m)
    formatInfo(value, unit) {
        return `${value / 10} ${unit}`;
    }

    // Conditions pour donner une couleur au fond de la carte en fonction de son type
    getColorType(type) {
        switch (type) {
            case 'Grass/Poison':
                return '#c5e1a5';
            case 'Fire':
                return '#ef9a9a';
            case 'Fire/Flying':
                return '#ffcc80';
            case 'Water':
                return '#bbdefb';
            default:
                return 'white';
        }
    }

    // méthode react (hook), s'exécute quand le component est chargé
    componentDidMount() {
        // récupère les données via l'url de App parent
        axios.get(this.props.url)
            .then(res => {
                this.setPokemonDetails(res.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    render() {
        return (
            <div>
                <Card sx={{ maxWidth: 345, backgroundColor: this.getColorType(this.state.pokemonDetail.types) }}>
                    <CardActionArea>
                        <CardContent>
                            <Typography gutterBottom variant="h4" component="div">
                                {this.state.pokemonDetail.name}
                            </Typography>
                            <Typography variant="h6" color="div">
                                {this.state.pokemonDetail.types}
                            </Typography>
                        </CardContent>
                        <CardMedia
                            component="img"
                            height="350"
                            image={this.state.pokemonDetail.img}
                            alt="pokemon"
                        />
                        <CardContent>
                            <Typography variant="h6" color="div">
                                Weight : {this.state.pokemonDetail.weight}
                            </Typography>
                            <Typography variant="h6" color="div">
                                Height : {this.state.pokemonDetail.height}
                            </Typography>
                        </CardContent>
                        <CardContent>
                            <Typography variant="h6" color="div">
                                Attack :
                            </Typography>
                            <Typography variant="h6" color="div">
                                Name : {this.state.attack1.name}
                            </Typography>
                            <Typography variant="h6" color="div">
                                Power (optional) : {this.state.attack1.power}
                            </Typography>
                            <Typography variant="h6" color="div">
                                Description : {this.state.attack1.description}
                            </Typography>
                        </CardContent>
                        <CardContent>
                            <Typography variant="h6" color="div">
                                Attack :
                            </Typography>
                            <Typography variant="h6" color="div">
                                Name : {this.state.attack2.name}
                            </Typography>
                            <Typography variant="h6" color="div">
                                Power (optional) : {this.state.attack2.power}
                            </Typography>
                            <Typography variant="h6" color="div">
                                Description : {this.state.attack2.description}
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
            </div>

        )
    }
}