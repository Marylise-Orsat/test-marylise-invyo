import React from 'react';
import axios from 'axios';
import PokemonDetail from './component/Detail';
import { Container, Grid } from '@mui/material';


export default class PokemonList extends React.Component {
  state = {
    pokemons: []
  }

  // ici on peut faire les requêtes réseaux et les initialisations qui requièrent l’existence de nœuds du DOM
  componentDidMount() {
    axios.get(`https://pokeapi.co/api/v2/pokemon?limit=9`)
      .then(res => {
        const pokemons = res.data.results;
        // setState : permet de modifier l'état du composant et d'en informer React (réafficher)
        // méthode du react component
        this.setState({ pokemons });
        // boucle les urls de chaque pokemon sortis sur la requête
        // console.log(this.state.pokemons.map(pokemon => pokemon.url))
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  // render examine en général this.props et this.state et renvoie des éléments react
  render() {
    return (
      <Container>
        <Grid container>
          <div>
            {this.state.pokemons.map(pokemon =>
              <PokemonDetail url={pokemon.url} />)}
          </div>
        </Grid>
      </Container>
    )
  }
}